#include <stdio.h>
#define SIZE 10

int sump(int * start, int * end);

int maina20(void)
{
    int marbles[SIZE] = { 20, 10, 5, 4, 3124, 22, 11234 };
    long answer;

    answer = sump(marbles, marbles + SIZE);
    printf("The total nubmer of maribles is %ld.\n", answer);

    return 0;
}

/* 使用指针算法 */
int sump(int * start, int * end)
{
    int total = 0;

    while (start < end)
    {
        total += *start;
        start++; // 指针指向下一个元素
    }

    return total;
}
