#include <stdio.h>
#include <ctype.h>

int maina11(void)
{

    char ch;

    while ((ch = getchar()) != '\n'){
        if (isalpha(ch))
            putchar(ch + 1); // 显示该字符的下一个字符
        else
            putchar(ch); // 原样显示
    }
    putchar(ch); // 显示换行符

    return 0;
}
