#include <stdio.h>
#define PRAISE "You are an extraordinary being."

int maina7(void)
{
    char name[40];

    printf("What's your name? ");
    scanf("%s", name);
    printf("Hello, %s, %s\n", name, PRAISE);
    printf("Your name of %lu letters cocupies %lu memory cells.\n",
           strlen(name), sizeof(name));
    printf("The pharse of praise has %lu letters ", strlen(PRAISE));
    printf("and occupies %lu memory cells.\n", sizeof(PRAISE));

    return 0;
}
